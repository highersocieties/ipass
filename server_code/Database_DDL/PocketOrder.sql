# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.35)
# Database: PocketOrder
# Generation Time: 2017-05-09 09:34:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Areas`;

CREATE TABLE `Areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Areas_name_uindex` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Areas_Users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Areas_Users`;

CREATE TABLE `Areas_Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_FK` int(11) NOT NULL,
  `area_FK` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Areas_Users_User_FK` (`area_FK`),
  CONSTRAINT `Areas_Users_Area_FK` FOREIGN KEY (`area_FK`) REFERENCES `Areas` (`id`),
  CONSTRAINT `Areas_Users_User_FK` FOREIGN KEY (`area_FK`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Orders`;

CREATE TABLE `Orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_FK` int(11) NOT NULL,
  `status_FK` int(11) NOT NULL,
  `timeOfPlacement` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Orders_OrderStatus_FK` (`status_FK`),
  CONSTRAINT `Orders_OrderStatus_FK` FOREIGN KEY (`status_FK`) REFERENCES `OrderStatus` (`id`),
  CONSTRAINT `Orders_Tables_FK` FOREIGN KEY (`id`) REFERENCES `Tables` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table OrderStatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `OrderStatus`;

CREATE TABLE `OrderStatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `OrderStatus_name_uindex` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Products`;

CREATE TABLE `Products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `unitPrice` decimal(10,0) NOT NULL,
  `taxPerc` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Products_SubOrders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Products_SubOrders`;

CREATE TABLE `Products_SubOrders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_FK` int(11) NOT NULL,
  `suborder_FK` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Products_SubOrders_Products_fk` (`product_FK`),
  KEY `Products_SubOrders_SubOrders_fk` (`suborder_FK`),
  CONSTRAINT `Products_SubOrders_Products_fk` FOREIGN KEY (`product_FK`) REFERENCES `Products` (`id`),
  CONSTRAINT `Products_SubOrders_SubOrders_fk` FOREIGN KEY (`suborder_FK`) REFERENCES `SubOrders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table SubOrders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `SubOrders`;

CREATE TABLE `SubOrders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_FK` int(11) NOT NULL,
  `type_FK` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `SubOrders_Orders_fk` (`order_FK`),
  KEY `SubOrders_SubOrderType_fk` (`type_FK`),
  CONSTRAINT `SubOrders_Order_FK` FOREIGN KEY (`id`) REFERENCES `Orders` (`id`),
  CONSTRAINT `SubOrders_SubOrderType_fk` FOREIGN KEY (`type_FK`) REFERENCES `SubOrderTypes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table SubOrderTypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `SubOrderTypes`;

CREATE TABLE `SubOrderTypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `SubOrderTypes_name_uindex` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Tables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Tables`;

CREATE TABLE `Tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_FK` int(11) NOT NULL,
  `numOfSeats` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Tables_Areas_FK` (`area_FK`),
  CONSTRAINT `Tables_Areas_FK` FOREIGN KEY (`area_FK`) REFERENCES `Areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table UserRoles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserRoles`;

CREATE TABLE `UserRoles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserRoles_name_uindex` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Users`;

CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `psswrd` varchar(40) DEFAULT NULL,
  `role_FK` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Users_UserRoles_fk` (`role_FK`),
  CONSTRAINT `Users_UserRoles_fk` FOREIGN KEY (`role_FK`) REFERENCES `UserRoles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
