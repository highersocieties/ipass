package com.karimStekelenburg.table.model;

import com.karimStekelenburg.area.model.Area;
import com.karimStekelenburg.order.model.Order;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "Tables", schema = "PocketOrder", catalog = "")
public class Table {
    private int id;
    private int numOfSeats;
    private Area area;
    private List<Order> orders;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "numOfSeats", nullable = false)
    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    @ManyToOne
    @JoinColumn(name = "area_FK")
    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @OneToMany(
            targetEntity = Order.class,
            mappedBy = "table",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Table table = (Table) o;

        if (id != table.id) return false;
        if (numOfSeats != table.numOfSeats) return false;
        if (area != null ? !area.equals(table.area) : table.area != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + numOfSeats;
//        result = 31 * result + (areasFk != null ? areasFk.hashCode() : 0);
        return result;
    }
}
