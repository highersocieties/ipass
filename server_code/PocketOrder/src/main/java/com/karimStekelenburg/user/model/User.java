package com.karimStekelenburg.user.model;


import com.karimStekelenburg.area.model.Area;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "Users", schema = "PocketOrder", catalog = "")
public class User {
    private int id;
    private String username;
    private String psswrd;
    private UserRole role;
    private boolean enabled;
    private List<Area> areas;

    public User() {
    }

    public User(String username, String psswrd, boolean enabled) {
        this.username = username;
        this.psswrd = psswrd;
        this.enabled = enabled;
    }

    public User(int id, String username, String psswrd, UserRole role, boolean enabled) {
        this.id = id;
        this.username = username;
        this.psswrd = psswrd;
        this.role = role;
        this.enabled = enabled;
    }

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "psswrd", nullable = true, length = 40)
    public String getPsswrd() {
        return psswrd;
    }

    public void setPsswrd(String psswrd) {
        this.psswrd = psswrd;
    }

    @ManyToOne
    @JoinColumn(name = "role_FK")
    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @ManyToMany
    @JoinTable(
            name = "Areas_Users",
            joinColumns = {@JoinColumn(name = "user_FK")},
            inverseJoinColumns = {@JoinColumn(name = "area_FK")}
    )
    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
//        if (roleFk != user.roleFk) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;
        if (psswrd != null ? !psswrd.equals(user.psswrd) : user.psswrd != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (psswrd != null ? psswrd.hashCode() : 0);
//        result = 31 * result + roleFk;
        return result;
    }
}
