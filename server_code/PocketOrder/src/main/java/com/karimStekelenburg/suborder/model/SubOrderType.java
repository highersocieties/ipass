package com.karimStekelenburg.suborder.model;


import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "SubOrderTypes", schema = "PocketOrder", catalog = "")
public class SubOrderType {
    private int id;
    private String name;
    private List<SubOrder> subOrders;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(
            targetEntity = SubOrder.class,
            mappedBy = "type",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    public List<SubOrder> getSubOrders() {
        return subOrders;
    }

    public void setSubOrders(List<SubOrder> subOrders) {
        this.subOrders = subOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubOrderType that = (SubOrderType) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
