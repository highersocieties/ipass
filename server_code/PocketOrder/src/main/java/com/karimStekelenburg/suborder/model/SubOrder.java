package com.karimStekelenburg.suborder.model;

import com.karimStekelenburg.order.model.Order;
import com.karimStekelenburg.product.model.Product;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "SubOrders", schema = "PocketOrder", catalog = "")
public class SubOrder {
    private int id;
    private int orderFk;
    private Order parentOrder;
    private SubOrderType type;
    private List<Product> products;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "order_FK", nullable = false)
    public int getOrderFk() {
        return orderFk;
    }

    public void setOrderFk(int orderFk) {
        this.orderFk = orderFk;
    }


    @ManyToOne
    @JoinColumn(name = "order_FK", insertable = false, updatable = false)
    public Order getParentOrder() {
        return parentOrder;
    }

    public void setParentOrder(Order parentOrder) {
        this.parentOrder = parentOrder;
    }

    @ManyToOne
    @JoinColumn(name = "type_FK")
    public SubOrderType getType() {
        return type;
    }

    public void setType(SubOrderType type) {
        this.type = type;
    }

    @ManyToMany
    @JoinTable(
            name = "Products_SubOrders",
            joinColumns = {@JoinColumn(name = "suborder_FK")},
            inverseJoinColumns = {@JoinColumn(name = "product_FK")}
    )
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubOrder subOrder = (SubOrder) o;

        if (id != subOrder.id) return false;
        if (orderFk != subOrder.orderFk) return false;
        if (type != subOrder.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + orderFk;
//        result = 31 * result + typeFk;
        return result;
    }
}
