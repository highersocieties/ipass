package com.karimStekelenburg.area.dao;

import com.karimStekelenburg.area.model.Area;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional

public interface AreaDao extends CrudRepository<Area, Integer> {

    /**
     * This method will find an User instance in the database by its email.
     * Note that this method is not implemented and its working code will be
     * automagically generated from its signature by Spring Data JPA.
     */
//    public Area findByEmail(String email);

}