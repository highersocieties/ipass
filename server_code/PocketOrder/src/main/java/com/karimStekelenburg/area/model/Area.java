package com.karimStekelenburg.area.model;

import com.karimStekelenburg.user.model.User;
import com.karimStekelenburg.table.model.Table;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "Areas", schema = "PocketOrder", catalog = "")
public class Area {
    private int id;
    private String name;
    private List<User> waiters;
    private List<Table> tables;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany
    @JoinTable(
            name = "Areas_Users",
            joinColumns = {@JoinColumn(name = "area_FK")},
            inverseJoinColumns = {@JoinColumn(name = "user_FK")}
    )
    public List<User> getWaiters() {
        return waiters;
    }

    public void setWaiters(List<User> waiters) {
        this.waiters = waiters;
    }

    @OneToMany(
            targetEntity = Table.class,
            mappedBy = "area",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;

        if (id != area.id) return false;
        if (name != null ? !name.equals(area.name) : area.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Area{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", waiters=" + waiters +
                '}';
    }
}
