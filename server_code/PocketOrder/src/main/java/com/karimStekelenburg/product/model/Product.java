package com.karimStekelenburg.product.model;

import com.karimStekelenburg.suborder.model.SubOrder;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "Products", schema = "PocketOrder", catalog = "")
public class Product {
    private int id;
    private String name;
    private int unitPrice;
    private int taxPerc;
    private List<SubOrder> subOrders;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "unitPrice", nullable = false, precision = 0)
    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "taxPerc", nullable = false, precision = 0)
    public int getTaxPerc() {
        return taxPerc;
    }

    public void setTaxPerc(int taxPerc) {
        this.taxPerc = taxPerc;
    }

    @ManyToMany
    @JoinTable(
            name = "Products_SubOrders",
            joinColumns = {@JoinColumn(name = "product_FK")},
            inverseJoinColumns = {@JoinColumn(name = "suborder_FK")}
    )
    public List<SubOrder> getSubOrders() {
        return subOrders;
    }

    public void setSubOrders(List<SubOrder> subOrders) {
        this.subOrders = subOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != product.id) return false;
        if (unitPrice != product.unitPrice) return false;
        if (taxPerc != product.taxPerc) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + unitPrice;
        result = 31 * result + taxPerc;
        return result;
    }
}
