package com.karimStekelenburg.config;

import java.util.Properties;
import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;

import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;;
import org.springframework.context.annotation.Import;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableWebMvc
@org.springframework.context.annotation.Configuration
@ComponentScan({ "com.karimStekelenburg.*" })
@EnableTransactionManagement
@Import({ SecurityConfig.class })
@EntityScan({ "com.karimStekelenburg.*" })
public class AppConfig {

    @Autowired
    SessionFactory sessionFactory;

    @Bean
    public SessionFactory sessionFactory() {



        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(com.karimStekelenburg.area.model.Area.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.order.model.Order.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.order.model.OrderStatus.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.product.model.Product.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.suborder.model.SubOrder.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.suborder.model.SubOrderType.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.table.model.Table.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.user.model.User.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.user.model.UserRole.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.user.model.UserRole.class);
        configuration.addAnnotatedClass(com.karimStekelenburg.user.model.UserRole.class);
        configuration.setPhysicalNamingStrategy(PhysicalNamingStrategyStandardImpl.INSTANCE);

        sessionFactory = configuration.buildSessionFactory();


        return sessionFactory;
    }

    private Properties getHibernateProperties() {
        Properties prop = new Properties();
        prop.put("hibernate.format_sql", "true");
        prop.put("hibernate.show_sql", "true");
        prop.put("hibernate.dialect",
                "org.hibernate.dialect.MySQL5Dialect");
        return prop;
    }

    @Bean(name = "dataSource")
    public BasicDataSource dataSource() {

        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://localhost:8889/PocketOrder");
        ds.setUsername("root");
        ds.setPassword("root");
        return ds;
    }

    //Create a transaction manager
    @Bean
    public HibernateTransactionManager txManager() {
        return new HibernateTransactionManager(sessionFactory());
    }

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver
                = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/pages/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

}