package com.karimStekelenburg.order.model;

import com.karimStekelenburg.suborder.model.SubOrder;

import com.karimStekelenburg.table.model.*;
import com.karimStekelenburg.table.model.Table;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@javax.persistence.Table(name = "Orders", schema = "PocketOrder", catalog = "")
public class Order {
    private int id;
    private int tableFk;
    private OrderStatus status;
    private Timestamp timeOfPlacement;
    private com.karimStekelenburg.table.model.Table table;
    private List<SubOrder> subOrders;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "table_FK", nullable = false)
    public int getTableFk() {
        return tableFk;
    }

    public void setTableFk(int tableFk) {
        this.tableFk = tableFk;
    }

    @ManyToOne
    @JoinColumn(name = "status_FK")
    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    @Basic
    @Column(name = "timeOfPlacement", nullable = false)
    public Timestamp getTimeOfPlacement() {
        return timeOfPlacement;
    }

    public void setTimeOfPlacement(Timestamp timeOfPlacement) {
        this.timeOfPlacement = timeOfPlacement;
    }

    @ManyToOne
    @JoinColumn(name = "table_FK", insertable = false, updatable = false)
    public com.karimStekelenburg.table.model.Table getTable() {
        return table;
    }

    public void setTable(com.karimStekelenburg.table.model.Table table) {
        this.table = table;
    }

    @OneToMany(
            targetEntity = SubOrder.class,
            mappedBy = "parentOrder",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    public List<SubOrder> getSubOrders() {
        return subOrders;
    }

    public void setSubOrders(List<SubOrder> subOrders) {
        this.subOrders = subOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (tableFk != order.tableFk) return false;
        if (status != order.status) return false;
        if (timeOfPlacement != null ? !timeOfPlacement.equals(order.timeOfPlacement) : order.timeOfPlacement != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + tableFk;
//        result = 31 * result + statusFk;
        result = 31 * result + (timeOfPlacement != null ? timeOfPlacement.hashCode() : 0);
        return result;
    }
}
