SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `Tables`;
DROP TABLE IF EXISTS `Orders`;
DROP TABLE IF EXISTS `Products`;
DROP TABLE IF EXISTS `Products_SubOrders`;
DROP TABLE IF EXISTS `Users`;
DROP TABLE IF EXISTS `Areas`;
DROP TABLE IF EXISTS `Area_Table`;
DROP TABLE IF EXISTS `UserRoles`;
DROP TABLE IF EXISTS `Areas_Users`;
DROP TABLE IF EXISTS `SubOrders`;
DROP TABLE IF EXISTS `OrderStatus`;
DROP TABLE IF EXISTS `SubOrderTypes`;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE `Tables` (
	`id` INTEGER NOT NULL,
	`numOfSeats` INTEGER NOT NULL,
	`area_FK` INTEGER NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Orders` (
	`id` INTEGER NOT NULL,
	`table_FK` INTEGER NOT NULL,
	`status_FK` INTEGER NOT NULL,
	`timeOfPlacement` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Products` (
	`id` INTEGER NOT NULL,
	`name` VARCHAR(20) NOT NULL,
	`unitPrice` NUMERIC NOT NULL,
	`tax` NUMERIC NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Products_SubOrders` (
	`id` INTEGER NOT NULL,
	`product_FK` INTEGER NOT NULL,
	`suborder_FK` INTEGER NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Users` (
	`id` INTEGER NOT NULL,
	`username` VARCHAR(15) NOT NULL,
	`password` VARCHAR(30) NOT NULL,
	`role_FK` INTEGER NOT NULL,
	`area_FK` INTEGER,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Areas` (
	`id` INTEGER NOT NULL,
	`name` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Area_Table` (
	`id` INTEGER NOT NULL,
	`table_FK` INTEGER NOT NULL,
	`area_FK` INTEGER NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `UserRoles` (
	`id` INTEGER NOT NULL,
	`name` VARCHAR(15) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Areas_Users` (
	`id` INTEGER NOT NULL,
	`user_FK` INTEGER NOT NULL,
	`area_FK` INTEGER NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `SubOrders` (
	`id` INTEGER NOT NULL,
	`order_FK` INTEGER NOT NULL,
	`type_FK` INTEGER NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `OrderStatus` (
	`id` INTEGER NOT NULL,
	`name` VARCHAR(15) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `SubOrderTypes` (
	`id` INTEGER NOT NULL,
	`name` VARCHAR(15) NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `Tables` ADD FOREIGN KEY (`area_FK`) REFERENCES `Areas`(`id`);
ALTER TABLE `Orders` ADD FOREIGN KEY (`table_FK`) REFERENCES `Tables`(`id`);
ALTER TABLE `Orders` ADD FOREIGN KEY (`status_FK`) REFERENCES `OrderStatus`(`id`);
ALTER TABLE `Products_SubOrders` ADD FOREIGN KEY (`product_FK`) REFERENCES `Products`(`id`);
ALTER TABLE `Products_SubOrders` ADD FOREIGN KEY (`suborder_FK`) REFERENCES `SubOrders`(`id`);
ALTER TABLE `Users` ADD FOREIGN KEY (`role_FK`) REFERENCES `UserRoles`(`id`);
ALTER TABLE `Users` ADD FOREIGN KEY (`area_FK`) REFERENCES `Areas`(`id`);
ALTER TABLE `Area_Table` ADD FOREIGN KEY (`table_FK`) REFERENCES `Tables`(`id`);
ALTER TABLE `Area_Table` ADD FOREIGN KEY (`area_FK`) REFERENCES `Areas`(`id`);
ALTER TABLE `Areas_Users` ADD FOREIGN KEY (`area_FK`) REFERENCES `Areas`(`id`);
ALTER TABLE `Areas_Users` ADD FOREIGN KEY (`user_FK`) REFERENCES `Users`(`id`);
ALTER TABLE `SubOrders` ADD FOREIGN KEY (`order_FK`) REFERENCES `Orders`(`id`);
ALTER TABLE `SubOrders` ADD FOREIGN KEY (`type_FK`) REFERENCES `SubOrderTypes`(`id`);